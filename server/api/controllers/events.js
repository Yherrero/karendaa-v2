const config = require('../assets/config')
const db = require('../models/db')

/**
 * Get all event from logged user
 *
 */
exports.get_all_events = (req, res) => {
  const userId = req.user._id

  const events = db.getEvents(userId)
  res.status(200).json({
    events
  })
}

/**
 * Get one event by id
 *
 */
exports.get_event_by_id = (req, res) => {
  const userId = req.user._id
  const eventId = req.params.eventId

  const event = db.getEvent(userId, eventId)

  if (typeof event === 'string') {
    return res.status(422).json({
      event
    })
  }

  res.status(200).json({
    event
  })
}

/**
 * Post a new event
 *
 * @param {string} title
 * @param {number} start
 * @param {number} end
 * @param {number} calendarId
 * @param {string} [description]
 */
exports.create_event = (req, res) => {
  const description = req.body.description
  const calendarId = +req.body.calendarId
  const title = req.body.title
  const start = req.body.start
  const end = req.body.end
  const userId = req.user._id

  if (!title || (!calendarId && calendarId !== 0) || !start || !end) {
    return res.status(config.errors.missingData.status).json({
      message: config.errors.missingData.message
    })
  }

  const errors = !description
    ? db.createEvent(userId, calendarId, title, start, end)
    : db.createEvent(userId, calendarId, title, start, end, description)

  if (errors.length) {
    res.status(config.errors.invalidData.status).json({
      message: config.errors.invalidData.message,
      errors
    })
  } else {
    const newEvent = db.getLastEvent()
    res.status(200).json({
      newEvent
    })
  }
}

/**
 * Edit an event
 *
 * @param {string} title
 * @param {number} start
 * @param {number} end
 * @param {number} calendarId
 * @param {string} description
 */
exports.edit_event_by_id = (req, res) => {
  const description = req.body.description || ''
  const title = req.body.title
  const start = req.body.start
  const end = req.body.end
  const eventId = req.params.eventId
  const userId = req.user._id

  if (!title || !start || !end) {
    return res.status(config.errors.missingData.status).json({
      message: config.errors.missingData.message
    })
  }

  const errors = db.editEvent(userId, eventId, title, start, end, description)

  if (errors.length) {
    res.status(config.errors.invalidData.status).json({
      message: config.errors.invalidData.message,
      errors
    })
  } else {
    res.status(200).json({
      message: 'Event edited'
    })
  }
}

/**
 * Delete an event
 *
 */
exports.delete_event_by_id = (req, res) => {
  const userId = req.user._id
  const eventId = req.params.eventId

  if (!eventId) {
    return res.status(config.errors.missingData.status).json({
      message: config.errors.missingData.message
    })
  }

  db.deleteEvent(userId, eventId)

  res.status(200).json({
    message: 'Event deleted'
  })
}
