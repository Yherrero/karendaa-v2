const passport = require('passport')
const passportJWT = require('passport-jwt')

const db = require('../models/db')

const ExtractJwt = passportJWT.ExtractJwt
const JwtStrategy = passportJWT.Strategy

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.JWT_KEY
}

const jwtStrategy = new JwtStrategy(jwtOptions, function(payload, next) {
  const user = db.getUserByEmailLight(payload.email)

  if (user) {
    next(null, {
      _id: user._id,
      email: user.email
    })
  } else {
    next(null, false)
  }
})

passport.use(jwtStrategy)

module.exports = passport.authenticate('jwt', { session: false })
