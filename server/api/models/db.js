let calendarIdCounter = 0
let eventIdCounter = 0
let userIdCounter = 0

let lastCalendarCreated = null
let lastEventCreated = null
let lastUserCreated = null

let users = []

// Create

/**
 * Create a new user
 *
 * @param {string} email
 * @param {string} password
 */
const createUser = (email, password) => {
  const errors = []

  if (isEmailExist(email)) errors.push('Email already taken')
  if (!isEmailValid(email)) errors.push('Invalid email')
  //if (!isPasswordValid(password)) errors.push('Invalid password')

  if (!errors.length) {
    const newUser = {
      _id: nextUserId(),
      email,
      password,
      calendars: []
    }
    users.push(newUser)
    lastUserCreated = newUser
  }

  return errors
}

/**
 * Create a new calendar
 *
 * @param {number} userId
 * @param {string} title
 * @param {string} [color]
 */
const createCalendar = (userId, title, color = '#bada55') => {
  const errors = []

  if (!isColorValid(color)) errors.push('Invalid color')
  if (!isValidTitle(title)) errors.push('Invalid title')

  const user = getUser(userId)

  if (!user) errors.push('Invalid userId')
  else {
    if (!errors.length) {
      newCalendar = {
        _id: nextCalendarId(),
        title,
        color,
        sharedWith: [],
        events: []
      }
      user.calendars.push(newCalendar)
      lastCalendarCreated = newCalendar
    }
  }

  return errors
}

/**
 * Create a new event
 *
 * @param {number} userId
 * @param {number} calendarId
 * @param {string} title
 * @param {number} start - timestamp
 * @param {number} end - timestamp
 * @param {string} [description]
 */
const createEvent = (
  userId,
  calendarId,
  title,
  start,
  end,
  description = ''
) => {
  const errors = []

  if (!isValidTitle(title)) errors.push('Invalid title')
  if (+start > +end) errors.push('Start must be before end')

  const user = getUser(userId)

  if (!user) errors.push('Invalid user id')
  else {
    const calendar = user.calendars.find(
      calendar => calendar._id === +calendarId
    )

    if (!calendar) errors.push('Invalid calendar or user id')
    else {
      if (!errors.length) {
        newEvent = {
          _id: nextEventId(),
          title,
          description,
          start,
          end
        }
        calendar.events.push(newEvent)
        lastEventCreated = newEvent
      }
    }
  }

  return errors
}

// Retrieve

/**
 * Get one user by id
 *
 * @param {number} userId
 */
const getUser = userId => users.find(user => user._id === +userId) || null

/**
 * Get one user by email
 *
 * @param {string} email
 */
const getUserByEmail = email => users.find(user => user.email === email) || null

/**
 * Get one user by email (without calendars)
 * @param {string} email
 */
const getUserByEmailLight = email =>
  users
    .map(user => {
      return {
        _id: user._id,
        email: user.email,
        password: user.password
      }
    })
    .find(user => user.email === email) || null

/**
 * Get all events form one user
 *
 * @param {number} userId
 */
const getEvents = userId =>
  getUser(userId).calendars.map(calendar => calendar.events) || []

/**
 * Get one event by id
 *
 * @param {number} userId
 * @param {number} eventId
 */
const getEvent = (userId, eventId) =>
  getUser(userId)
    .calendars.map(calendars => calendars.events)
    .reduce((flat, arr) => flat.concat(arr), [])
    .find(event => event._id === +eventId) || 'Wrong user or event id'

// Update

/**
 * Edit a calendar
 *
 * @param {number} userId
 * @param {number} calendarId
 * @param {string} title
 * @param {string} color
 */
const editCalendar = (userId, calendarId, title, color) => {
  const errors = []

  if (!isColorValid(color)) errors.push('Invalid color')
  if (!isValidTitle(title)) errors.push('Invalid title')

  const user = getUser(userId)

  if (!user) errors.push('Invalid userId')
  else {
    const calendar = user.calendars.find(
      calendar => calendar._id === +calendarId
    )
    if (!calendar) errors.push('Invalid calendar id')
    else {
      if (!errors.length) {
        calendar.color = color
        calendar.title = title
      }
    }
  }
  return errors
}

/**
 * Edit an event
 *
 * @param {number} userId
 * @param {number} eventId
 * @param {string} title
 * @param {number} start
 * @param {number} end
 * @param {string} description
 */
const editEvent = (userId, eventId, title, start, end, description) => {
  const errors = []

  const event = getEvent(userId, eventId)

  if (typeof event === 'string') errors.push(event)
  else {
    if (!isValidTitle(title)) errors.push('Invalid title')
    if (+start > +end) errors.push('Start must be before end')

    if (!errors.length) {
      event.title = title
      event.start = start
      event.end = end
      event.description = description
    }
  }
  return errors
}

// Delete

/**
 * Delete a calendar
 *
 * @param {number} userId
 * @param {number} calendarId
 */
const deleteCalendar = (userId, calendarId) => {
  const user = getUser(userId)
  user.calendars = user.calendars.filter(
    calendar => calendar._id !== +calendarId
  )
}

/**
 * Delete an event
 *
 * @param {number} userId
 * @param {number} eventId
 */
const deleteEvent = (userId, eventId) =>
  getUser(userId).calendars.forEach(calendar => {
    calendar.events = calendar.events.filter(event => event._id !== +eventId)
  })

// Getters

/**
 * Get last calendar created
 */
const getLastCalendar = () => lastCalendarCreated

/**
 * Get last event created
 */
const getLastEvent = () => lastEventCreated

/**
 * Get last user created
 */
const getLastUser = () => lastUserCreated

// Helpers

const isPasswordValid = password =>
  /^[A-Za-z0-9]{6,}$/.test(password) &&
  /[A-Z]+/.test(password) &&
  /[a-z]+/.test(password) &&
  /[0-9]+/.test(password)

const isEmailExist = email =>
  users.find(user => user.email === email) !== undefined

const isEmailValid = email =>
  /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(
    email
  )

const isColorValid = color => /^#[0-9A-F]{6}$/i.test(color)

const isValidTitle = s => /^[0-9a-z '-]+$/i.test(s)

const nextCalendarId = () => calendarIdCounter++

const nextEventId = () => eventIdCounter++

const nextUserId = () => userIdCounter++

const db = {
  createUser,
  createCalendar,
  createEvent,
  getUser,
  getUserByEmail,
  getUserByEmailLight,
  getEvents,
  getEvent,
  editCalendar,
  editEvent,
  deleteCalendar,
  deleteEvent,
  getLastCalendar,
  getLastEvent,
  getLastUser
  // users // == /!\ == Only for testing purposes
}

// == /!\ == Only for testing purposes
// const dbFill = require('../assets/dbFill')
// dbFill(db)

module.exports = db
