const bcrypt = require('bcryptjs')

const logErrors = (...arr) =>
  arr.forEach(a => (a.length ? console.log(a) : null))

module.exports = db => {
  bcrypt.hash('q1w2E3', 10, (err, hash) => {
    logErrors(
      db.createUser('yannick@mail.com', hash),
      db.createUser('david@mail.com', hash),
      db.createUser('morgane@mail.com', hash),
      db.createUser('benjamin@mail.com', hash),

      db.createCalendar(0, 'Birthdays'),
      db.createCalendar(1, 'Birthdays'),
      db.createCalendar(0, 'Courses', '#ad5642'),

      db.createEvent(0, 0, "Benjamin's birthday", 1561626000000, 1561636000000),
      db.createEvent(0, 0, "Morgane's birthday", 1565172000000, 1565172000000),
      db.createEvent(0, 0, "David's birthday", 1563872400000, 1563876000000),
      db.createEvent(0, 2, 'Js course', 1550046600000, 1550061000000, 'K116'),
      db.createEvent(0, 2, 'English', 1549888200000, 1549899000000, 'A26'),
      db.editCalendar(0, 0, 'title edited', '#f0f0f0'),
      db.editEvent(0, 0, 'title edited', 1, 2, 'desc edited')
    )
  })
}
