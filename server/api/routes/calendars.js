const express = require('express')

const CalendarsController = require('../controllers/calendars')
const checkAuth = require('../middlewares/check-auth')

const router = express.Router()

/**
 * Create a new calendar for logged user
 *
 */
router.post('', checkAuth, CalendarsController.create_calendar)

/**
 * Edit a calendar
 *
 */
router.patch('/:calendarId', checkAuth, CalendarsController.edit_calendar_by_id)

/**
 * Delete a calendar
 *
 */
router.delete(
  '/:calendarId',
  checkAuth,
  CalendarsController.delete_calendar_by_id
)

module.exports = router
