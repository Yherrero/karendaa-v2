const express = require('express')

const UsersController = require('../controllers/users')
const checkAuth = require('../middlewares/check-auth')

const router = express.Router()

/**
 * Create a new user
 *
 */
router.post('/signup', UsersController.signup_user)

/**
 * Log a user in
 *
 */
router.post('/login', UsersController.login_user)

/**
 * Get all user infos
 *
 */
router.get('/', checkAuth, UsersController.retrieve_user) // auth

// == /!\ == Only for testing purposes
// router.get('/all', UsersController.get_all_users)

module.exports = router
