export default {
  state: {
    currentView: 'monthly',
    sidebarVisible: true,
    addEventVisible: false,
    addCalendarVisible: false
  },
  mutations: {
    changeCurrentView(state, payload) {
      state.currentView = payload
    },
    toggleSidebar(state) {
      state.sidebarVisible = !state.sidebarVisible
    },
    toggleAddEvent(state) {
      state.addEventVisible = !state.addEventVisible
    },
    toggleAddCalendar(state) {
      state.addCalendarVisible = !state.addCalendarVisible
    }
  },
  actions: {
    changeCurrentView({ commit }, view) {
      commit('changeCurrentView', view)
    },
    toggleSidebar({ commit }) {
      commit('toggleSidebar')
    },
    toggleAddEvent({ commit }) {
      commit('toggleAddEvent')
    },
    toggleAddCalendar({ commit }) {
      commit('toggleAddCalendar')
    }
  },
  getters: {
    currentView: state => state.currentView,
    sidebarVisible: state => state.sidebarVisible,
    addEventVisible: state => state.addEventVisible,
    addCalendarVisible: state => state.addCalendarVisible
  }
}
