import MyDate from '@/assets/js/myDate'

export default {
  state: {
    currentDate: null
  },
  mutations: {
    setDate(state, payload) {
      state.currentDate = payload
    }
  },
  actions: {
    initDate({ commit }) {
      const date = new MyDate(new Date())
      commit('setDate', date)
    },
    setDate({ commit }, date) {
      const verifDate = new Date(date)
      verifDate.setHours(23, 59)
      if (verifDate.valueOf() > Date.now()) {
        const newDate = new MyDate(date)
        commit('setDate', newDate)
      }
    }
  },
  getters: {
    date(state) {
      return state.currentDate
    },
    yearDays(state) {
      return state.currentDate.getYearDays()
    },
    monthDays(state) {
      return state.currentDate.getMonthDays()
    },
    weekDays(state) {
      return state.currentDate.getWeekDays()
    }
  }
}
