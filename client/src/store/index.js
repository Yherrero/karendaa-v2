import Vue from 'vue'
import Vuex from 'vuex'

import date from './date'
import user from './user'
import shared from './shared'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    date,
    user,
    shared
  }
})
