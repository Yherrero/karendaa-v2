export default class MyDate {
  constructor(date) {
    this.date = date
  }

  // === GETTERS ===

  /**
   * Return the current date
   */
  getDate() {
    return this.date
  }

  /**
   * Return first day of the week (monday)
   */
  getWeekStart() {
    const d = this.clone(this.date)
    const dayOfWeek = d.getDay()
    const diff = d.getDate() - dayOfWeek + (dayOfWeek === 0 ? -6 : 1)
    const res = new Date(d.setDate(diff))
    return new Date(res.getFullYear(), res.getMonth(), res.getDate())
  }

  /**
   * Return an array with all the days of the week
   */
  getWeekDays() {
    const days = []
    const start = this.getWeekStart()
    for (let i = 0; i < 7; i++) {
      let date = this.clone(start)
      date.setDate(start.getDate() + i)
      days.push(date)
    }
    return days
  }

  /**
   * Return the first day of the month
   */
  getMonthStart() {
    return new Date(this.date.getFullYear(), this.date.getMonth())
  }

  /**
   * Return the last day of the month
   */
  getMonthEnd() {
    const end2 = new Date(this.date.getFullYear(), this.date.getMonth() + 1)
    end2.setDate(0)
    return end2
  }

  /**
   * Return an array with all the days of the month
   */
  getMonthDays() {
    let days = []
    let end = this.getMonthEnd()
    let start = this.getMonthStart()
    // days from the last week of previous month
    let dayOfWeek = start.getDay() - 1
    if (dayOfWeek === -1) {
      dayOfWeek = 6
    }
    if (dayOfWeek > 0) {
      for (let i = dayOfWeek; i > 0; i--) {
        let date = this.clone(start)
        date.setDate(start.getDate() - i)
        days.push(date)
      }
    }
    // current month days
    for (let i = 0; i < end.getDate(); ++i) {
      let date = this.clone(start)
      date.setDate(i + 1)
      days.push(date)
    }
    // firsts days from  next month
    dayOfWeek = end.getDay() - 1
    if (dayOfWeek === -1) {
      dayOfWeek = 6
    }
    if (dayOfWeek < 6) {
      for (let i = 0; i < 6 - dayOfWeek; i++) {
        let date = this.clone(end)
        date.setDate(end.getDate() + i + 1)
        days.push(date)
      }
    }
    return days
  }

  /**
   * Return the french name of the current month
   */
  getMonthName() {
    return this.date.toLocaleString('fr-fr', { month: 'long' })
  }

  /**
   * Return first day of the current year
   */
  getYearStart() {
    return new Date(this.date.getFullYear().toString(), 0)
  }

  /**
   * Return last day of the current year
   */
  getYearEnd() {
    return new Date(this.date.getFullYear().toString(), 11, 31)
  }

  /**
   * Return an array that contains 12 arrays, one for each month
   * each arrays contains all the days of the month
   */
  getYearDays() {
    const currentDate = this.clone(this.date)
    const months = []
    this.setDate(this.getYearStart())
    for (let i = 0; i < 12; i++) {
      months.push(this.getMonthDays())
      this.nextMonth()
    }
    this.setDate(currentDate)
    return months
  }

  // === SETTERS ===

  /**
   * Set the current date to date
   * @param {Date} date
   */
  setDate(date) {
    this.date = date
  }

  /**
   * Set the current date hours to hour
   * @param {Number} hour
   */
  setHours(hour) {
    this.date.setHours(hour)
  }

  /**
   * Set the current date minutes to minutes
   * @param {Number} minutes
   */
  setMinutes(minutes) {
    this.date.setMinutes(minutes)
  }

  /**
   * Set the current date to the fist day of the next week
   */
  nextWeek() {
    const newDate = new Date(
      this.getWeekStart().getTime() + 7 * 24 * 60 * 60 * 1000
    )
    this.setDate(newDate)
  }

  /**
   * Set the current date to the fist day of the previous week
   */
  previousWeek() {
    const newDate = new Date(
      this.getWeekStart().getTime() - 7 * 24 * 60 * 60 * 1000
    )
    this.setDate(newDate)
  }

  /**
   * Set the current date to the first day of the next month
   */
  nextMonth() {
    let nextMonth = this.date.getMonth() + 1
    let nextYear = this.date.getFullYear()
    if (nextMonth > 11) {
      nextMonth = 0
      nextYear++
    }
    const newDate = new Date(nextYear, nextMonth)
    this.setDate(newDate)
  }

  /**
   * Set the current date to the first day of the previous month
   */
  previousMonth() {
    let nextMonth = this.date.getMonth() - 1
    let nextYear = this.date.getFullYear()
    if (nextMonth < 0) {
      nextMonth = 11
      nextYear--
    }
    const newDate = new Date(nextYear, nextMonth)
    this.setDate(newDate)
  }

  /**
   * Set the current date to the first day of the next year
   */
  nextYear() {
    const newDate = new Date((this.date.getFullYear() + 1).toString(), 0)
    this.setDate(newDate)
  }

  /**
   * Set the current date to the first day of the previous year
   */
  previousYear() {
    const newDate = new Date((this.date.getFullYear() - 1).toString(), 0)
    this.setDate(newDate)
  }

  // === HELPERS ===

  /**
   * Return true if current month contains the date
   * @param {Date} date
   */
  contains(date) {
    return date.getMonth() === this.date.getMonth()
  }

  /**
   * Return a clone of the date
   * @param {Date} date
   */
  clone(date) {
    return new Date(date.getTime())
  }
}
