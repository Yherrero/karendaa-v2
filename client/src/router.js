import Vue from 'vue'
import Router from 'vue-router'
import store from './store/'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Signup from './views/Signup.vue'
import Calendar from './views/Calendar.vue'
import AddEvent from './views/AddEvent.vue'
import EventDetails from './views/EventDetails.vue'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      meta: {
        layout: 'default',
        requiresAuth: true
      },
      component: Calendar
    },
    {
      path: '/home',
      meta: {
        layout: 'extended-header',
        requiresGuest: true
      },
      component: Home
    },
    {
      path: '/event/add',
      meta: {
        layout: 'no-sidebar',
        requiresAuth: true
      },
      component: AddEvent
    },
    {
      path: '/event/:id',
      meta: {
        layout: 'no-sidebar',
        requiresAuth: true
      },
      component: EventDetails
    },
    {
      path: '/login',
      meta: {
        layout: 'extended-header',
        requiresGuest: true
      },
      component: Login
    },
    {
      path: '/signup',
      meta: {
        layout: 'extended-header',
        requiresGuest: true
      },
      component: Signup
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.loggedIn) {
      next()
      return
    }
    next('/home')
  } else if (to.matched.some(record => record.meta.requiresGuest)) {
    if (store.getters.loggedIn) {
      next('/')
      return
    }
    next()
  } else {
    next()
  }
})

export default router
