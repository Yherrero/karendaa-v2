import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/'
import axios from 'axios'
// import './registerServiceWorker'

Vue.config.productionTip = false

axios.defaults.baseURL = 'https://karendaa-api.herokuapp.com'
// axios.defaults.baseURL = 'http://127.0.0.1:3000'

Vue.prototype.$http = axios

const token = localStorage.getItem('token')

if (token) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] = token
}

new Vue({
  router,
  store,
  render: h => h(App),
  created() {
    if (token) {
      this.$store.dispatch('loadUser').catch(err => {
        console.log('Token expired : ', err)
        this.$store.dispatch('logout').then(() => {
          this.$router.push('/home')
        })
      })
    }
  }
}).$mount('#app')
