<Block>

# calendars

</Block>

<!-- POST CALENDARS/ -->

<Block>

## (post) calendars/

You can use this API to create new calendar to the logged user.  
(auth needed)

### Endpoint

```bash
POST /calendars/
```

| Name  |  Type  | Description |      Required      |
| :---: | :----: | :---------: | :----------------: |
| title | string |    title    | :heavy_check_mark: |
| color | string |    color    | :heavy_minus_sign: |

### Response

```json
Status: 200

"newCalendar": {
        "_id": 1,
        "title": "my title",
        "color": "#bada55",
        "sharedWith": [],
        "events": []
    }
```

<Example>

<CURL>
```bash
curl -X POST https://karendaa-api.herokuapp.com/calendars/ \
  --data '{
    "title": "my title",
    "color": "#bada55"
  }'
```
</CURL>

</Example>

</Block>

<!-- PATCH CALENDARS/:id -->

<Block>

## (patch) calendars/:calendarId

You can use this API to edit a calendar.  
(auth nedded)

### Endpoint

```bash
PATCH /calendars/:calendarId
```

| Name  |  Type  | Description |      Required      |
| :---: | :----: | :---------: | :----------------: |
| title | string |    title    | :heavy_check_mark: |
| color | string |    color    | :heavy_check_mark: |

### Response

```json
Status: 200

{
    "message": "Calendar edited"
}
```

<Example>

<CURL>
```bash
curl -X PATCH https://karendaa-api.herokuapp.com/calendar/:calendarId \
  --data '{
    "title": "my title edited",
    "color": "#2942c3"
  }'
```
</CURL>

</Example>

</Block>

<!-- DELETE CALENDARS/:calendarId -->

<Block>

## (patch) calendars/:calendarId

You can use this API to delete a calendar.  
(auth nedded)

### Endpoint

```bash
DELETE /calendars/:calendarId
```

### Response

```json
Status: 200

{
    "message": "Calendar deleted"
}
```

<Example>

<CURL>
```bash
curl -X DELETE https://karendaa-api.herokuapp.com/calendar/:calendarId
```
</CURL>

</Example>

</Block>
